# coding=utf-8
# import
from flask import request
from neuralnetworkcommon.service.connectionPooledResource import ConnectionPooledResource
from inspect import signature
from neuralnetworkcommon.database.workspace import insert, update, selectAllIds, deleteAll, selectById, deleteById
from pythoncommontools.objectUtil.objectUtil import methodArgsStringRepresentation
from pythoncommontools.service.service import Action, inputLogFormator, loadRequest, dumpResponse, outputLogFormator, responseError, errorLogFormator, application
from pythoncommontools.objectUtil.POPO import loadFromDict
# global workspace resource
class GlobalWorkspace(ConnectionPooledResource):
    # create a workspace
    def post(self):
        # input log
        method = Action.POST
        resource = methodArgsStringRepresentation( signature( GlobalWorkspace.post ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # parse parameters
        dictWorkspace = loadRequest(request)
        workspace = loadFromDict(dictWorkspace)
        # insert training set
        response = None
        connection = ConnectionPooledResource.connectionPool.getconn()
        try:
            # insert training set
            cursor = connection.cursor()
            insert(cursor,workspace)
            connection.commit()
            # encode response
            response = workspace.id
            response = dumpResponse(workspace.id)
            # output log
            message = outputLogFormator(method, resource)
            application.logger.info(message)
        except Exception as exception:
            connection.rollback()
            response = responseError(500,exception)
            message = errorLogFormator(method, resource, exception)
            application.logger.error(message)
        finally:
            cursor.close()
            ConnectionPooledResource.connectionPool.putconn(connection)
        return response
    # update a workspace
    def put(self):
        # input log
        method = Action.PUT
        resource = methodArgsStringRepresentation( signature( GlobalWorkspace.put ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # parse parameters
        dictWorkspace = loadRequest(request)
        workspace = loadFromDict(dictWorkspace)
        # update workspace
        response = None
        connection = ConnectionPooledResource.connectionPool.getconn()
        try:
            # update perceptron
            cursor = connection.cursor()
            update(cursor,workspace)
            connection.commit()
            # output log
            message = outputLogFormator(method, resource)
            application.logger.info(message)
        except Exception as exception:
            connection.rollback()
            response = responseError(500,exception)
            message = errorLogFormator(method, resource, exception)
            application.logger.error(message)
        finally:
            cursor.close()
            ConnectionPooledResource.connectionPool.putconn(connection)
        return response
    # get all workspaces IDs
    def get(self):
        # input log
        method = Action.GET
        resource = methodArgsStringRepresentation( signature( GlobalWorkspace.get ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # resource process
        connection = ConnectionPooledResource.connectionPool.getconn()
        cursor = connection.cursor()
        loadedWorkspaceIds = selectAllIds(cursor)
        cursor.close()
        ConnectionPooledResource.connectionPool.putconn(connection)
        response = dumpResponse(loadedWorkspaceIds)
        # output log & return
        message = outputLogFormator(method, resource)
        application.logger.info(message)
        return response
    # delete a workspace
    def delete(self):
        # input log
        method = Action.DELETE
        resource = methodArgsStringRepresentation( signature( GlobalWorkspace.delete ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # resource process
        connection = ConnectionPooledResource.connectionPool.getconn()
        cursor = connection.cursor()
        deleteAll(cursor)
        connection.commit()
        cursor.close()
        ConnectionPooledResource.connectionPool.putconn(connection)
    pass
# specific workspace resource
class SpecificWorkspace(ConnectionPooledResource):
    # select a workspace
    def get(self,workspaceId):
        # input log
        method = Action.GET
        resource = methodArgsStringRepresentation( signature( SpecificWorkspace.get ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        response = None
        connection = ConnectionPooledResource.connectionPool.getconn()
        cursor = connection.cursor()
        rawWorkspace = selectById(cursor,workspaceId)
        cursor.close()
        ConnectionPooledResource.connectionPool.putconn(connection)
        jsonWorkspace = None
        if rawWorkspace:
            jsonWorkspace = rawWorkspace.dumpToDict()
        # encode response
        response = dumpResponse(jsonWorkspace)
        # output log & return
        message = outputLogFormator(method, resource)
        application.logger.info(message)
        return response
    # delete a workspace
    def delete(self,workspaceId):
        # input log
        method = Action.DELETE
        resource = methodArgsStringRepresentation( signature( SpecificWorkspace.delete ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # resource process
        connection = ConnectionPooledResource.connectionPool.getconn()
        cursor = connection.cursor()
        deleteById(cursor,workspaceId)
        connection.commit()
        cursor.close()
        ConnectionPooledResource.connectionPool.putconn(connection)
        pass
    pass
pass
