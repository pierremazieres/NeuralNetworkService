# coding=utf-8
# import
from flask import request
from neuralnetworkcommon.service.connectionPooledResource import ConnectionPooledResource
from inspect import signature
from neuralnetworkcommon.database.perceptron import insert, update, patch, selectAllIdsByWorkspaceId, deleteAllByWorkspaceId, selectById, deleteById, selectSummaryById
from neuralnetworkcommon.service.service import CommonParameters
from pythoncommontools.objectUtil.objectUtil import methodArgsStringRepresentation
from pythoncommontools.service.service import Action, inputLogFormator, loadRequest, dumpResponse, outputLogFormator, responseError, errorLogFormator, application
from pythoncommontools.objectUtil.POPO import loadFromDict
# global perceptron resource
# TODO : delete or make optionnal the comments
class GlobalPerceptron(ConnectionPooledResource):
    # create a perceptron
    def post(self):
        # input log
        method = Action.POST
        resource = methodArgsStringRepresentation( signature( GlobalPerceptron.post ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # parse parameters
        dictPerceptron = loadRequest(request)
        perceptron = loadFromDict(dictPerceptron)
        # insert perceptron
        response = None
        connection = ConnectionPooledResource.connectionPool.getconn()
        try:
            # insert perceptron
            cursor = connection.cursor()
            insert(cursor,perceptron)
            connection.commit()
            response = dumpResponse(perceptron.id)
            # output log
            message = outputLogFormator(method, resource)
            application.logger.info(message)
        except Exception as exception:
            connection.rollback()
            response = responseError(500,exception)
            message = errorLogFormator(method, resource, exception)
            application.logger.error(message)
        finally:
            cursor.close()
            ConnectionPooledResource.connectionPool.putconn(connection)
        return response
    # update a perceptron
    def put(self):
        # input log
        method = Action.PUT
        resource = methodArgsStringRepresentation( signature( GlobalPerceptron.put ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # parse parameters
        dictPerceptron = loadRequest(request)
        perceptron = loadFromDict(dictPerceptron)
        # update perceptron
        response = None
        connection = ConnectionPooledResource.connectionPool.getconn()
        try:
            # update perceptron
            cursor = connection.cursor()
            update(cursor,perceptron)
            connection.commit()
            # output log
            message = outputLogFormator(method, resource)
            application.logger.info(message)
        except Exception as exception:
            connection.rollback()
            response = responseError(500,exception)
            message = errorLogFormator(method, resource, exception)
            application.logger.error(message)
        finally:
            cursor.close()
            ConnectionPooledResource.connectionPool.putconn(connection)
        return response
    # patch a perceptron
    def patch(self):
        # input log
        method = Action.PATCH
        resource = methodArgsStringRepresentation( signature( GlobalPerceptron.patch ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # parse parameters
        dictPerceptron = loadRequest(request)
        perceptron = loadFromDict(dictPerceptron)
        # patch perceptron
        response = None
        connection = ConnectionPooledResource.connectionPool.getconn()
        try:
            cursor = connection.cursor()
            patch(cursor,perceptron)
            connection.commit()
            # output log
            message = outputLogFormator(method, resource)
            application.logger.info(message)
        except Exception as exception:
            connection.rollback()
            response = responseError(500,exception)
            message = errorLogFormator(method, resource, exception)
            application.logger.error(message)
        finally:
            cursor.close()
            ConnectionPooledResource.connectionPool.putconn(connection)
        return response
    # get all perceptrons IDs
    def get(self):
        # input log
        method = Action.GET
        resource = methodArgsStringRepresentation( signature( GlobalPerceptron.get ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # resource process
        workspaceId = parameters[CommonParameters.WORKSPACE_ID.value] if CommonParameters.WORKSPACE_ID.value in parameters else None
        connection = ConnectionPooledResource.connectionPool.getconn()
        cursor = connection.cursor()
        loadedPerceptronIds = selectAllIdsByWorkspaceId(cursor,workspaceId)
        cursor.close()
        ConnectionPooledResource.connectionPool.putconn(connection)
        response = dumpResponse(loadedPerceptronIds)
        # output log & return
        message = outputLogFormator(method, resource)
        application.logger.info(message)
        return response
    # delete a perceptron
    def delete(self):
        # input log
        method = Action.DELETE
        resource = methodArgsStringRepresentation( signature( GlobalPerceptron.delete ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # resource process
        workspaceId = parameters[CommonParameters.WORKSPACE_ID.value] if CommonParameters.WORKSPACE_ID.value in parameters else None
        connection = ConnectionPooledResource.connectionPool.getconn()
        cursor = connection.cursor()
        deleteAllByWorkspaceId(cursor,workspaceId)
        connection.commit()
        cursor.close()
        ConnectionPooledResource.connectionPool.putconn(connection)
    pass
# specific perceptron resource
class SpecificPerceptron(ConnectionPooledResource):
    # select a perceptron
    def get(self,perceptronId):
        # input log
        method = Action.GET
        resource = methodArgsStringRepresentation( signature( GlobalPerceptron.get ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        response = None
        connection = ConnectionPooledResource.connectionPool.getconn()
        cursor = connection.cursor()
        rawPerceptron = selectById(cursor,perceptronId)
        cursor.close()
        ConnectionPooledResource.connectionPool.putconn(connection)
        jsonPerceptron = None
        if rawPerceptron:
            jsonPerceptron = rawPerceptron.dumpToDict()
        # encode response
        response = dumpResponse(jsonPerceptron)
        # output log & return
        message = outputLogFormator(method, resource)
        application.logger.info(message)
        return response
    # delete a perceptron
    def delete(self,perceptronId):
        # input log
        method = Action.DELETE
        resource = methodArgsStringRepresentation( signature( SpecificPerceptron.delete ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # resource process
        connection = ConnectionPooledResource.connectionPool.getconn()
        cursor = connection.cursor()
        deleteById(cursor,perceptronId)
        connection.commit()
        cursor.close()
        ConnectionPooledResource.connectionPool.putconn(connection)
# perceptron summary resource
class PerceptronSummary(ConnectionPooledResource):
    def get(self,perceptronId):
        # input log
        method = Action.GET
        resource = methodArgsStringRepresentation( signature( PerceptronSummary.get ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # resource process
        connection = ConnectionPooledResource.connectionPool.getconn()
        cursor = connection.cursor()
        rawPerceptronSummary = selectSummaryById(cursor,perceptronId)
        cursor.close()
        ConnectionPooledResource.connectionPool.putconn(connection)
        jsonPerceptronSummary = None
        if rawPerceptronSummary:
            jsonPerceptronSummary = rawPerceptronSummary.dumpToDict()
        response = dumpResponse(jsonPerceptronSummary)
        # output log & return
        message = outputLogFormator(method, resource)
        application.logger.info(message)
        return response
    pass
# perceptron execution
class PerceptronExecution(ConnectionPooledResource):
    def put(self,perceptronId):
        # input log
        method = Action.PUT
        resource = methodArgsStringRepresentation( signature( PerceptronExecution.put ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # parse parameters
        inputVector = loadRequest(request)
        # update perceptron
        response = None
        connection = ConnectionPooledResource.connectionPool.getconn()
        try:
            # execute perceptron
            cursor = connection.cursor()
            perceptron = selectById(cursor,perceptronId,True)
            outputVector = perceptron.layerTrainingDB.passForward(inputVector)
            outputVector = [float(_) for _ in outputVector]
            response = dumpResponse(outputVector)
            # output log
            message = outputLogFormator(method, resource)
            application.logger.info(message)
        except Exception as exception:
            response = responseError(500,exception)
            message = errorLogFormator(method, resource, exception)
            application.logger.error(message)
        finally:
            perceptron.layerTrainingDB.cleanup()
            cursor.close()
            ConnectionPooledResource.connectionPool.putconn(connection)
        return response
    pass
pass
