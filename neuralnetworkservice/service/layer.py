# coding=utf-8
# import
from flask import request
from neuralnetworkcommon.service.connectionPooledResource import ConnectionPooledResource
from inspect import signature
from neuralnetworkcommon.database.layer import insert, update, selectAllIdsByPerceptronId, deleteAllByPerceptronId, selectByPerceptronIdAndDepthIndex, deleteByPerceptronIdAndDepthIndex, selectSummaryByPerceptronIdAndDepthIndex
from neuralnetworkcommon.service.service import CommonParameters
from pythoncommontools.objectUtil.objectUtil import methodArgsStringRepresentation
from pythoncommontools.service.service import Action, inputLogFormator, loadRequest, dumpResponse, outputLogFormator, responseError, errorLogFormator, application
from pythoncommontools.objectUtil.POPO import loadFromDict
# global layer resource
# TODO : delete or make optionnal the comments
class GlobalLayer(ConnectionPooledResource):
    # create a layer
    def post(self):
        # input log
        method = Action.POST
        resource = methodArgsStringRepresentation( signature( GlobalLayer.post ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # parse parameters
        previousDimension = int(parameters[CommonParameters.PREVIOUS_DIMENSION.value]) if CommonParameters.PREVIOUS_DIMENSION.value in parameters else None
        currentDimension = int(parameters[CommonParameters.CURRENT_DIMENSION.value]) if CommonParameters.CURRENT_DIMENSION.value in parameters else None
        dictLayer = loadRequest(request)
        layer = loadFromDict(dictLayer)
        # insert layer
        response = None
        connection = ConnectionPooledResource.connectionPool.getconn()
        try:
            # insert layer
            cursor = connection.cursor()
            insert(cursor,layer, previousDimension, currentDimension)
            connection.commit()
            # output log
            message = outputLogFormator(method, resource)
            application.logger.info(message)
        except Exception as exception:
            connection.rollback()
            response = responseError(500,exception)
            message = errorLogFormator(method, resource, exception)
            application.logger.error(message)
        finally :
            cursor.close()
            ConnectionPooledResource.connectionPool.putconn(connection)
        return response
    # update a layer
    def put(self):
        # input log
        method = Action.PUT
        resource = methodArgsStringRepresentation( signature( GlobalLayer.put ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # parse parameters
        dictLayer = loadRequest(request)
        layer = loadFromDict(dictLayer)
        # update layer
        response = None
        connection = ConnectionPooledResource.connectionPool.getconn()
        try:
            # update layer
            cursor = connection.cursor()
            update(cursor,layer)
            connection.commit()
            # output log
            message = outputLogFormator(method, resource)
            application.logger.info(message)
        except Exception as exception:
            connection.rollback()
            response = responseError(500,exception)
            message = errorLogFormator(method, resource, exception)
            application.logger.error(message)
        finally:
            cursor.close()
            ConnectionPooledResource.connectionPool.putconn(connection)
        return response
    # read a layer
    def get(self):
        # input log
        method = Action.GET
        resource = methodArgsStringRepresentation( signature( GlobalLayer.get ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # resource process
        perceptronId = parameters[CommonParameters.PERCEPTRON_ID.value] if CommonParameters.PERCEPTRON_ID.value in parameters else None
        connection = ConnectionPooledResource.connectionPool.getconn()
        cursor = connection.cursor()
        loadedLayerIds = selectAllIdsByPerceptronId(cursor,perceptronId)
        cursor.close()
        ConnectionPooledResource.connectionPool.putconn(connection)
        response = dumpResponse(loadedLayerIds)
        # output log & return
        message = outputLogFormator(method, resource)
        application.logger.info(message)
        return response
    # delete a layer
    def delete(self):
        # input log
        method = Action.DELETE
        resource = methodArgsStringRepresentation( signature( GlobalLayer.delete ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # resource process
        perceptronId = parameters[CommonParameters.PERCEPTRON_ID.value] if CommonParameters.PERCEPTRON_ID.value in parameters else None
        connection = ConnectionPooledResource.connectionPool.getconn()
        cursor = connection.cursor()
        deleteAllByPerceptronId(cursor,perceptronId)
        connection.commit()
        cursor.close()
        ConnectionPooledResource.connectionPool.putconn(connection)
    pass
# specific layer resource
class SpecificLayer(ConnectionPooledResource):
    # select a layer
    def get(self,perceptronId,depthIndex):
        # input log
        method = Action.GET
        resource = methodArgsStringRepresentation( signature( GlobalLayer.get ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        response = None
        connection = ConnectionPooledResource.connectionPool.getconn()
        cursor = connection.cursor()
        rawLayer = selectByPerceptronIdAndDepthIndex(cursor,perceptronId,depthIndex)
        cursor.close()
        ConnectionPooledResource.connectionPool.putconn(connection)
        jsonLayer = None
        if rawLayer:
            jsonLayer = rawLayer.dumpToDict()
        # encode response
        response = dumpResponse(jsonLayer)
        # output log & return
        message = outputLogFormator(method, resource)
        application.logger.info(message)
        return response
    # delete a layer
    def delete(self,perceptronId,depthIndex):
        # input log
        method = Action.DELETE
        resource = methodArgsStringRepresentation( signature( SpecificLayer.delete ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # resource process
        connection = ConnectionPooledResource.connectionPool.getconn()
        cursor = connection.cursor()
        deleteByPerceptronIdAndDepthIndex(cursor,perceptronId,depthIndex)
        connection.commit()
        cursor.close()
        ConnectionPooledResource.connectionPool.putconn(connection)
# layer summary resource
class LayerSummary(ConnectionPooledResource):
    def get(self,perceptronId, depthIndex):
        # input log
        method = Action.GET
        resource = methodArgsStringRepresentation( signature( LayerSummary.get ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # resource process
        connection = ConnectionPooledResource.connectionPool.getconn()
        cursor = connection.cursor()
        rawLayerSummary = selectSummaryByPerceptronIdAndDepthIndex(cursor,perceptronId, depthIndex)
        cursor.close()
        ConnectionPooledResource.connectionPool.putconn(connection)
        jsonLayerSummary = None
        if rawLayerSummary:
            jsonLayerSummary = rawLayerSummary.dumpToDict()
        response = dumpResponse(jsonLayerSummary)
        # output log & return
        message = outputLogFormator(method, resource)
        application.logger.info(message)
        return response
    pass
pass
