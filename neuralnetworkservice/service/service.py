# coding=utf-8
# import
from neuralnetworkcommon.service.service import ResourcePathType
from neuralnetworkservice.service.perceptron import GlobalPerceptron, SpecificPerceptron, PerceptronSummary, PerceptronExecution
from neuralnetworkservice.service.layer import GlobalLayer, SpecificLayer, LayerSummary
from neuralnetworkservice.service.workspace import GlobalWorkspace, SpecificWorkspace
from os import environ
from pythoncommontools.service.service import API, application, log
from pythoncommontools.service.httpSymbol import HttpSymbol
# contants
endpoint=environ.get("NEURALNETWORK_SERVICE_ENDPOINT")
logLevel=environ.get("NEURALNETWORK_SERVICE_LOG_LEVEL")
# initialize service
API.add_resource(GlobalWorkspace, HttpSymbol.PATH_SEPARATOR.value.join(('',endpoint,ResourcePathType.GLOBAL_WORKSPACE.value,)))
API.add_resource(SpecificWorkspace, HttpSymbol.PATH_SEPARATOR.value.join(('',endpoint,ResourcePathType.SPECIFIC_WORKSPACE.value,)))
API.add_resource(GlobalPerceptron, HttpSymbol.PATH_SEPARATOR.value.join(('',endpoint,ResourcePathType.GLOBAL_PERCEPTRON.value,)))
API.add_resource(SpecificPerceptron, HttpSymbol.PATH_SEPARATOR.value.join(('',endpoint,ResourcePathType.SPECIFIC_PERCEPTRON.value,)))
API.add_resource(PerceptronSummary, HttpSymbol.PATH_SEPARATOR.value.join(('',endpoint,ResourcePathType.PERCEPTRON_SUMMARY.value,)))
API.add_resource(PerceptronExecution, HttpSymbol.PATH_SEPARATOR.value.join(('',endpoint,ResourcePathType.PERCEPTRON_EXECUTION.value,)))
API.add_resource(GlobalLayer, HttpSymbol.PATH_SEPARATOR.value.join(('',endpoint,ResourcePathType.GLOBAL_LAYER.value,)))
API.add_resource(SpecificLayer, HttpSymbol.PATH_SEPARATOR.value.join(('',endpoint,ResourcePathType.SPECIFIC_LAYER.value,)))
API.add_resource(LayerSummary, HttpSymbol.PATH_SEPARATOR.value.join(('',endpoint,ResourcePathType.LAYER_SUMMARY.value,)))
# set log
application.logger.setLevel(logLevel)
log.setLevel(logLevel)
