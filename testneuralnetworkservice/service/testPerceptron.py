#!/usr/bin/env python3
# coding=utf-8
# import
from gzip import compress, decompress
from json import loads, dumps
from neuralnetworkcommon.entity.perceptron.perceptron import Perceptron
from neuralnetworkservice.service.service import endpoint
from numpy.random import rand
from random import choice
from string import ascii_letters
from testneuralnetworkcommon.commonUtilities import CommonTest, randomPerceptron, insertRandomPerceptrons, insertRandomLayers
from testpythoncommontools.service.service import checkRestServerNotDefaultError
from neuralnetworkcommon.database.layer import selectByPerceptronIdAndDepthIndex, deleteAllByPerceptronId
from pythoncommontools.objectUtil.POPO import loadFromDict
from testneuralnetworkservice.service.service import TestService, clientApplication
from neuralnetworkcommon.service.service import globalPerceptronUrl, specificPerceptronUrl, perceptronSummaryUrl, perceptronExecutionUrl
# test perceptron
compressedBadPerceptron = compress(Perceptron('',None).dumpToSimpleJson().encode())
globalResource = globalPerceptronUrl(endpoint)
class testPerceptronWS(TestService):
    # test CRUD OK
    def testCrudOK(self):
        # initial perceptron
        rawInitialPerceptron = randomPerceptron()
        CommonTest.connection.commit()
        dumpedInitialPerceptron = rawInitialPerceptron.dumpToSimpleJson()
        compressedInitialPerceptron = compress(dumpedInitialPerceptron.encode())
        # precheck
        self.assertIsNone(rawInitialPerceptron.id,"ERROR : perceptron has id")
        # create perceptron
        response = clientApplication.post(globalResource,data=compressedInitialPerceptron)
        perceptronId = int(decompress(response.data))
        # check creation
        self.assertIsNotNone(perceptronId,"ERROR : perceptron has no id")
        rawInitialPerceptron.id = perceptronId
        specificResource = specificPerceptronUrl(endpoint,perceptronId)
        # read perceptron
        response = clientApplication.get(specificResource)
        jsonPerceptron = loads(decompress(response.data).decode())
        fetchedInsertedPerceptron = loadFromDict(jsonPerceptron)
        # check reading
        self.assertEqual(rawInitialPerceptron,fetchedInsertedPerceptron,"ERROR : inserted perceptron does not match")
        # update perceptron
        rawNewPerceptron = randomPerceptron(perceptronId)
        dumpedNewPerceptron = rawNewPerceptron.dumpToSimpleJson()
        compressedNewPerceptron = compress(dumpedNewPerceptron.encode())
        clientApplication.put(globalResource,data=compressedNewPerceptron)
        # check update
        response = clientApplication.get(specificResource)
        jsonPerceptron = loads(decompress(response.data).decode())
        fetchedUpdatedPerceptron = loadFromDict(jsonPerceptron)
        self.assertNotEqual(fetchedUpdatedPerceptron,fetchedInsertedPerceptron,"ERROR : perceptron not updated")
        rawNewPerceptron.id = perceptronId
        self.assertEqual(fetchedUpdatedPerceptron,rawNewPerceptron,"ERROR : updated perceptron does not match")
        # summarize perceptron
        layersNumber = insertRandomLayers(perceptronId,True)
        CommonTest.connection.commit()
        summaryResource = perceptronSummaryUrl(endpoint,perceptronId)
        response = clientApplication.get(summaryResource)
        jsonPerceptronSummary = loads(decompress(response.data).decode())
        perceptronSummary = loadFromDict(jsonPerceptronSummary)
        # check summary
        self.assertEqual(perceptronSummary.id,fetchedUpdatedPerceptron.id,"ERROR : perceptron summary id does not match")
        self.assertEqual(perceptronSummary.workspaceId,fetchedUpdatedPerceptron.workspaceId,"ERROR : perceptron summary workspaceId does not match")
        self.assertEqual(perceptronSummary.comments,fetchedUpdatedPerceptron.comments,"ERROR : perceptron summary comments does not match")
        self.assertEqual(perceptronSummary.layersNumber,layersNumber,"ERROR : perceptron summary layers does not match")
        # patch perceptron
        updatedComments = ''.join([choice(ascii_letters) for _ in range(50)])
        patchData = Perceptron(perceptronId,TestService.testWorkspaceId, updatedComments)
        dumpedPatchData = patchData.dumpToSimpleJson()
        compressedPatchData = compress(dumpedPatchData.encode())
        clientApplication.patch(globalResource,data=compressedPatchData)
        # check patch
        response = clientApplication.get(specificResource)
        jsonPerceptron = loads(decompress(response.data).decode())
        fetchedPatchedPerceptron = loadFromDict(jsonPerceptron)
        self.assertEqual(fetchedPatchedPerceptron.workspaceId,fetchedUpdatedPerceptron.workspaceId,"ERROR : patched perceptron workspaceId not updated")
        self.assertNotEqual(fetchedPatchedPerceptron.comments,fetchedUpdatedPerceptron.comments,"ERROR : patched perceptron comments not updated")
        self.assertEqual(fetchedPatchedPerceptron.comments,updatedComments,"ERROR : patched perceptron comments does not match")
        # execute perceptron
        firstLayer = selectByPerceptronIdAndDepthIndex(CommonTest.cursor,perceptronId,0)
        inputVectorLength = len(firstLayer.weights[0])
        lastLayer = selectByPerceptronIdAndDepthIndex(CommonTest.cursor,perceptronId,layersNumber-1)
        expectedOutputVectorLength = len(lastLayer.weights)
        rawInputVector = [float(_) for _ in rand(inputVectorLength) ]
        compressedInputVector = compress(dumps(rawInputVector).encode())
        executionResource = perceptronExecutionUrl(endpoint,perceptronId)
        response = clientApplication.put(executionResource, data=compressedInputVector)
        deleteAllByPerceptronId(CommonTest.cursor,perceptronId)
        CommonTest.connection.commit()
        # check execution
        outputVector = loads(decompress(response.data).decode())
        actualOutputVectorLength = len(outputVector)
        self.assertEqual(expectedOutputVectorLength,actualOutputVectorLength,"ERROR : perceptron outpout does not fit")
        # delete perceptron
        clientApplication.delete(specificResource)
        # check deletion
        response = clientApplication.get(specificResource)
        jsonPerceptron = loads(decompress(response.data).decode())
        self.assertIsNone(jsonPerceptron,"ERROR : perceptron not deleted")
        pass
    # test select/delete all OK
    def testSelectDeleteAll(self):
        # initialize random perceptrons
        perceptronIds=insertRandomPerceptrons()
        CommonTest.connection.commit()
        # select IDs
        globalRessourceWorkspaceId = globalPerceptronUrl(endpoint,CommonTest.testWorkspaceId)
        response = clientApplication.get(globalRessourceWorkspaceId)
        fetchedIds = loads(decompress(response.data).decode())
        # check IDs
        self.assertTrue(perceptronIds.issubset(fetchedIds),"ERROR : IDs selection does not match")
        # delete all
        clientApplication.delete(globalResource)
        # check IDs
        response = clientApplication.get(globalResource)
        deletedIds = loads(decompress(response.data).decode())
        self.assertEqual(len(deletedIds),0,"ERROR : complete deletion failed")
        pass
    # test error
    def testPostDataError(self):
        checkRestServerNotDefaultError(self, clientApplication.post ,globalResource, compressedBadPerceptron)
    def testPutDataError(self):
        checkRestServerNotDefaultError(self, clientApplication.put ,globalResource, compressedBadPerceptron)
    def testPatchDataError(self):
        checkRestServerNotDefaultError(self, clientApplication.patch ,globalResource, compressedBadPerceptron)
    def testPerceptronExecutionError(self):
        # generate perceptron
        insertRandomLayers(CommonTest.testPerceptronId, True)
        CommonTest.connection.commit()
        specificResource = perceptronExecutionUrl(endpoint,CommonTest.testPerceptronId)
        rawInitialPerceptron = randomPerceptron()
        dumpedInitialPerceptron = rawInitialPerceptron.dumpToSimpleJson()
        compressedInitialPerceptron = compress(dumpedInitialPerceptron.encode())
        clientApplication.put(specificResource,data=compressedInitialPerceptron)
        # execute perceptron
        firstLayer = selectByPerceptronIdAndDepthIndex(CommonTest.cursor,CommonTest.testPerceptronId,0)
        inputVectorLength = len(firstLayer.weights[0])+1
        rawInputVector = [float(_) for _ in rand(inputVectorLength) ]
        compressedInputVector = compress(dumps(rawInputVector).encode())
        # check execution
        checkRestServerNotDefaultError(self, clientApplication.put ,specificResource, compressedInputVector)
        pass
    pass
pass
