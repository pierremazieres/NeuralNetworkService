#!/usr/bin/env python3
# coding=utf-8
# import
from gzip import compress, decompress
from json import loads
from neuralnetworkcommon.entity.workspace import Workspace
from neuralnetworkservice.service.service import endpoint
from testneuralnetworkcommon.commonUtilities import randomWorkspace, insertRandomWorkspaces
from testpythoncommontools.service.service import checkRestServerNotDefaultError
from pythoncommontools.objectUtil.POPO import loadFromDict
from neuralnetworkcommon.service.service import specificWorkspaceUrl, globalWorkspaceUrl
from testneuralnetworkservice.service.service import TestService, clientApplication
from testneuralnetworkcommon.commonUtilities import CommonTest
# test workspace
globalResource = globalWorkspaceUrl(endpoint)
rawBadWorkspace = Workspace('')
del rawBadWorkspace.comments
compressedBadWorkspace = compress(rawBadWorkspace.dumpToSimpleJson().encode())
class testWorkspaceWS(TestService):
    # test CRUD OK
    def testCrudOK(self):
        # randomize initial workspace
        rawInitialWorkspace = randomWorkspace()
        dumpedInitialWorkspace = rawInitialWorkspace.dumpToSimpleJson()
        compressedInitialWorkspace = compress(dumpedInitialWorkspace.encode())
        # precheck
        self.assertIsNone(rawInitialWorkspace.id,"ERROR : workspace has id")
        # create workspace
        response = clientApplication.post(globalResource,data=compressedInitialWorkspace)
        workspaceId = int(decompress(response.data))
        # check creation
        self.assertIsNotNone(workspaceId,"ERROR : workspace has no id")
        rawInitialWorkspace.id = workspaceId
        specificResource = specificWorkspaceUrl(endpoint,workspaceId)
        # read workspace
        response = clientApplication.get(specificResource)
        jsonWorkspace = loads(decompress(response.data).decode())
        fetchedInsertedWorkspace = loadFromDict(jsonWorkspace)
        # check reading
        self.assertEqual(rawInitialWorkspace,fetchedInsertedWorkspace,"ERROR : inserted workspace does not match")
        # update workspace
        rawNewWorkspace = randomWorkspace(workspaceId)
        dumpedNewWorkspace = rawNewWorkspace.dumpToSimpleJson()
        compressedNewWorkspace = compress(dumpedNewWorkspace.encode())
        clientApplication.put(globalResource,data=compressedNewWorkspace)
        # check update
        response = clientApplication.get(specificResource)
        jsonWorkspace = loads(decompress(response.data).decode())
        fetchedUpdatedWorkspace = loadFromDict(jsonWorkspace)
        self.assertNotEqual(fetchedUpdatedWorkspace,fetchedInsertedWorkspace,"ERROR : workspace not updated")
        rawNewWorkspace.id = workspaceId
        self.assertEqual(fetchedUpdatedWorkspace,rawNewWorkspace,"ERROR : updated workspace does not match")
        # delete workspace
        clientApplication.delete(specificResource)
        # check deletion
        response = clientApplication.get(specificResource)
        jsonWorkspace = loads(decompress(response.data).decode())
        self.assertIsNone(jsonWorkspace,"ERROR : workspace not deleted")
        pass
    # test select/delete all OK
    def testSelectDeleteAll(self):
        # initialize random workspaces
        initialIds=insertRandomWorkspaces()
        CommonTest.connection.commit()
        # select IDs
        response = clientApplication.get(globalResource)
        fetchedIds = loads(decompress(response.data).decode())
        # check IDs
        self.assertTrue(initialIds.issubset(fetchedIds),"ERROR : IDs selection does not match")
        ''' INFO : we do not check the "delete all" functionality in order to avoid :
         - delete other people workspace
         - error with existing workspace containing perceptrons or training sets'''
        # delete all
        for initialId in initialIds:
            specificResource = specificWorkspaceUrl(endpoint, initialId)
            clientApplication.delete(specificResource)
        # check IDs
        response = clientApplication.get(globalResource)
        remainingIds = loads(decompress(response.data).decode())
        self.assertNotIn(initialIds,remainingIds, "ERROR : IDs selection does not match")
        pass
    # test error
    ''' UNABLE TO REPRODUCE
    def testPostDataError(self):
        checkRestServerNotDefaultError(self, clientApplication.post, globalResource, compressedBadWorkspace)
    '''
    def testPutDataError(self):
        checkRestServerNotDefaultError(self, clientApplication.put, globalResource, compressedBadWorkspace)
    pass
pass
