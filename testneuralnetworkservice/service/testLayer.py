#!/usr/bin/env python3
# coding=utf-8
# import
from gzip import compress, decompress
from json import loads
from neuralnetworkcommon.entity.layer.layer import Layer
from neuralnetworkservice.service.service import endpoint
from testneuralnetworkcommon.commonUtilities import CommonTest, randomLayer, insertRandomLayers
from testpythoncommontools.service.service import checkRestServerNotDefaultError
from random import randint
from pythoncommontools.objectUtil.POPO import loadFromDict
from testneuralnetworkservice.service.service import TestService, clientApplication
from neuralnetworkcommon.service.service import globalLayerUrl, specificLayerUrl, layerSummaryUrl
# test layer
compressedBadLayer = compress(Layer('',None).dumpToSimpleJson().encode())
globalResource = globalLayerUrl(endpoint)
class testLayerWS(TestService):
    # test CRUD OK
    def testCrudOK(self):
        # initial layer
        depthIndex = 0
        rawInitialLayer = randomLayer(CommonTest.testPerceptronId,depthIndex)
        CommonTest.connection.commit()
        dumpedInitialLayer = rawInitialLayer.dumpToSimpleJson()
        compressedInitialLayer = compress(dumpedInitialLayer.encode())
        initialPreviousDimension = randint(2,5)
        initialCurrentDimension = randint(3,9)
        # create layer
        insertResource = globalLayerUrl(endpoint,previousDimension=initialPreviousDimension,currentDimension=initialCurrentDimension)
        clientApplication.post(insertResource,data=compressedInitialLayer)
        # check creation
        specificResource = specificLayerUrl(endpoint,CommonTest.testPerceptronId,depthIndex)
        # read layer
        response = clientApplication.get(specificResource)
        jsonLayer = loads(decompress(response.data).decode())
        fetchedInsertedLayer = loadFromDict(jsonLayer)
        # check reading
        self.assertEqual(depthIndex,fetchedInsertedLayer.depthIndex,"ERROR : inserted layer depthIndex does not match")
        self.assertEqual(CommonTest.testPerceptronId,fetchedInsertedLayer.perceptronId,"ERROR : inserted layer perceptronId does not match")
        self.assertEqual(initialCurrentDimension,len(fetchedInsertedLayer.weights),"ERROR : inserted layer does not match")
        self.assertEqual(initialPreviousDimension,len(fetchedInsertedLayer.weights[0]),"ERROR : inserted layer does not match")
        self.assertEqual(initialCurrentDimension,len(fetchedInsertedLayer.biases),"ERROR : inserted layer biases does not match")
        # update layer
        updatedPreviousDimension = randint(12,15)
        updatedCurrentDimension = randint(13,19)
        rawNewLayer = randomLayer(CommonTest.testPerceptronId,depthIndex,updatedPreviousDimension,updatedCurrentDimension)
        dumpedNewLayer = rawNewLayer.dumpToSimpleJson()
        compressedNewLayer = compress(dumpedNewLayer.encode())
        clientApplication.put(globalResource,data=compressedNewLayer)
        # check update
        response = clientApplication.get(specificResource)
        jsonLayer = loads(decompress(response.data).decode())
        fetchedUpdatedLayer = loadFromDict(jsonLayer)
        self.assertNotEqual(fetchedUpdatedLayer,fetchedInsertedLayer,"ERROR : layer not updated")
        self.assertEqual(depthIndex,fetchedUpdatedLayer.depthIndex,"ERROR : updated layer depthIndex does not match")
        self.assertEqual(CommonTest.testPerceptronId,fetchedUpdatedLayer.perceptronId,"ERROR : updated layer perceptronId does not match")
        self.assertEqual(updatedCurrentDimension,len(fetchedUpdatedLayer.weights),"ERROR : updated layer does not match")
        self.assertEqual(updatedPreviousDimension,len(fetchedUpdatedLayer.weights[0]),"ERROR : updated layer does not match")
        self.assertEqual(updatedCurrentDimension,len(fetchedUpdatedLayer.biases),"ERROR : updated layer biases does not match")
        # summarize layer
        summaryResource = layerSummaryUrl(endpoint,CommonTest.testPerceptronId,depthIndex)
        response = clientApplication.get(summaryResource)
        jsonLayerSummary = loads(decompress(response.data).decode())
        layerSummary = loadFromDict(jsonLayerSummary)
        # check summary
        self.assertEqual(layerSummary.perceptronId,CommonTest.testPerceptronId,"ERROR : layer summary perceptronId does not match")
        self.assertEqual(layerSummary.depthIndex,depthIndex,"ERROR : layer summary depthIndex does not match")
        self.assertEqual(layerSummary.weightsDimensions,[updatedCurrentDimension,updatedPreviousDimension],"ERROR : layer summary weightsDimensions does not match")
        self.assertEqual(layerSummary.biasesDimension,updatedCurrentDimension,"ERROR : layer summary initialCurrentDimension does not match")
        # delete layer
        clientApplication.delete(specificResource)
        # check deletion
        response = clientApplication.get(specificResource)
        jsonLayer = loads(decompress(response.data).decode())
        self.assertIsNone(jsonLayer,"ERROR : layer not deleted")
        pass
    # test select/delete all OK
    def testSelectDeleteAll(self):
        # initialize random layers
        layerNumbers=insertRandomLayers(CommonTest.testPerceptronId)
        CommonTest.connection.commit()
        # select IDs
        globalRessourcePerceptronId = globalLayerUrl(endpoint,perceptronId=CommonTest.testPerceptronId)
        response = clientApplication.get(globalRessourcePerceptronId)
        fetchedIds = loads(decompress(response.data).decode())
        # check IDs
        self.assertLessEqual(layerNumbers,len(fetchedIds),"ERROR : IDs selection does not match")
        # delete all
        clientApplication.delete(globalResource)
        # check IDs
        response = clientApplication.get(globalResource)
        deletedIds = loads(decompress(response.data).decode())
        self.assertEqual(len(deletedIds),0,"ERROR : complete deletion failed")
        pass
    # test error
    def testPostDataError(self):
        checkRestServerNotDefaultError(self, clientApplication.post ,globalResource, compressedBadLayer)
    def testPutDataError(self):
        checkRestServerNotDefaultError(self, clientApplication.put ,globalResource, compressedBadLayer)
    pass
pass
